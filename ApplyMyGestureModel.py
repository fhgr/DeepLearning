#!/usr/bin/env python
# coding: utf-8

# In[1]:


#%% imports
from __future__ import print_function
from __future__ import division
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy
print("PyTorch Version: ",torch.__version__)
print("Torchvision Version: ",torchvision.__version__)
import glob


# In[2]:


#%% Inputs

# camera ID used for opencv
DEVICE_ID = 0

# Top level data directory. Here we assume the format of the directory conforms
#   to the ImageFolder structure
data_dir = "." # "./data/hymenoptera_data"
data_dir = r"C:\python\WPy64-3850\notebooks\_notebooks\DeepLearning\myThumbsGesturesData"

# Models to choose from [resnet, alexnet, vgg, squeezenet, densenet, inception]
model_name = "squeezenet"
# any of [resnet, alexnet, vgg, squeezenet, densenet, inception]

# input_size: size of images to be put into the model
input_size = 224

# Number of classes in the dataset
files = glob.glob(os.path.join(data_dir,'train','*'))
num_classes = len(files)
print(f'Number of classes found to train: {num_classes}')

# Detect if we have a GPU available
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# model path and filename
# model_filename = 'trained_squeezenet_model_gpu.pytorch'
model_filename = 'trained_squeezenet_model.pytorch'


# In[6]:
    
# load model mapping it to the available hardware
model_ft = torch.load(model_filename, map_location=torch.device(device))
# Print the model we just instantiated
print(model_ft)




# In[23]:


class_names = [os.path.basename(x) for x in files]
print(class_names)


# In[25]:


# https://towardsdatascience.com/object-detection-and-tracking-in-pytorch-b3cf1a696a98
from torch.autograd import Variable

img_size=input_size
conf_thres=0.8
nms_thres=0.4

def detect_image(model, img):
    # scale and pad image
    ratio = min(img_size/img.size[0], img_size/img.size[1])
    imw = round(img.size[0] * ratio)
    imh = round(img.size[1] * ratio)
#    'val': transforms.Compose([
#        transforms.Resize(input_size),
#        transforms.CenterCrop(input_size),
#        transforms.ToTensor(),
#        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
#    ]),
    
    img_transforms=transforms.Compose([transforms.Resize((imh,imw)),
         transforms.Pad((max(int((imh-imw)/2),0), 
              max(int((imw-imh)/2),0), max(int((imh-imw)/2),0),
              max(int((imw-imh)/2),0)), (128,128,128)),
         transforms.ToTensor(),
         ])
    img_transforms=transforms.Compose([transforms.Resize(input_size),
         transforms.CenterCrop(input_size),
         transforms.ToTensor(),
         transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
         ])
    # convert image to Tensor
    image_tensor = img_transforms(img).float()
    image_tensor = image_tensor.unsqueeze_(0)
    input_img = Variable(image_tensor.type(torch.Tensor))
    # run inference on the model and get detections
    with torch.no_grad():
        outputs = model(input_img)
        _, preds = torch.max(outputs, 1)
        outputs = np.array(model(input_img)[0])
        outputs = outputs/np.sum(outputs)
        # print(outputs)
        # print(class_names)
    return int(preds[0]), outputs



# In[34]:

import cv2
def getImageFromWebcam():
    cap = cv2.VideoCapture(DEVICE_ID)
    if not cap.isOpened():
        raise IOError('Could not open Video Camera')
    ret, frame = cap.read()
    cap.release()
    if not ret:
        raise IOError('Could not read from Video Camera')
    plt.figure()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    return(frame)

frame = getImageFromWebcam()
plt.imshow(frame)


# In[35]:


from PIL import Image
img = Image.fromarray(frame)
predClass, probabilities = detect_image(model_ft,img)
resultText = f'{class_names[predClass]} ({probabilities[predClass]*100:3.1f}%)'
print(resultText)
plt.imshow(frame)
plt.title(resultText)


# In[24]:
        
cap = cv2.VideoCapture(DEVICE_ID)
if not cap.isOpened():
    raise IOError('Could not open Video Camera')

# font
font = cv2.FONT_HERSHEY_SIMPLEX
# org
org = (20, 30)
# fontScale
fontScale = 1
# Blue color in BGR
color = (32, 232, 32)
# Line thickness of 2 px
thickness = 2
   

# As display, we start with an empty image
finalImage = np.zeros((480,900,3), np.dtype('uint8'))+255
# and add the thumbsup-icon to the right of the image
thumbsUp = cv2.imread('ThumbsUp.png')
if thumbsUp is None:
    print('WARNING: ThumbsUp.png-Icon could not be loaded')
else:
    finalImage[10:235,675:,:] = thumbsUp;
    thumbsDown = cv2.flip(thumbsUp, 0);
    finalImage[245:470,675:,:] = thumbsDown;

try:
    while(True):
        ret, frame = cap.read()
        if not ret:
            raise IOError('Could not read from Video Camera')
        # Resize image frame to 640 x 480
        if(len(frame.shape)<3): # grayscale image
            frame = cv2.resize(np.dstack((frame,frame,frame)), (640,480));
        else:
            frame = cv2.resize(frame, (640,480));
        # cut out quadratic section (480 x 480 pixels) 
        img = Image.fromarray(cv2.cvtColor(frame[:,80:480+80,:], cv2.COLOR_BGR2RGB))
        
        # perform inception (predict object class and return probabilities)
        predClass, probabilities = detect_image(model_ft,img)
        
        image = finalImage.copy()
        image[:,0:640,:] = frame;
        image[10:471,650:665,:]=0;
        image[11:470,651:664,:]=232;
        
        preditionThreshold = 0.55 # at least 55% probability required
        if(probabilities[predClass] < preditionThreshold): # probability below threshold
            predClass = 0;    # overwrite with default class (NoThumbs)
        txt = class_names[predClass]
        image = cv2.putText(image, txt, org, font, 
                       fontScale, (0,0,0), thickness+1, cv2.LINE_AA)
        image = cv2.putText(image, txt, org, font, 
                       fontScale, color, thickness, cv2.LINE_AA)
        if predClass > 0: # we have ThumbsUp or ThumbsDown
            # draw a black stripe with the length of val
            val = np.minimum(230*np.maximum(probabilities[1],probabilities[2]),230);
            print(f'{int(val/4.8):d}: {240+val}, {240-val}   ', end='')
            print(probabilities)
            if probabilities[1]>probabilities[2]: # Thumbs Down
                print("Thumbs are really down")
                image[240:int(240+val),650:665,0:2]=0;
            else:                 # Thumbs Up
                print("Thumbs are UPUPUP")
                image[int(240-val):240,650:665,0:2]=0;
        cv2.imshow("prediction", image)
        ch = cv2.waitKey(10)
        if ch == ord('q'):
            break
        if ch == 27:
            break
except:
    pass
cv2.destroyAllWindows()
cap.release()
    

# In[ ]:




